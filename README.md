# h5bp & bootstrap

Empty h5bp ([https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)) with ant 
build script ([https://github.com/h5bp/ant-build-script](https://github.com/h5bp/ant-build-script)) and
twitter bootstrap ([https://github.com/twbs/bootstrap](https://github.com/twbs/bootstrap)).

Start with a new html5boilerplate together with twitter bootstrap to create fancy new websites.

Use ant build tool when finishing the development phase to create a lightweight version of the site.
The 'publish' directory is then ready for production!

## How to use:

Open terminal at root of repos and run

   $ cd build/
   
   $ ant build


## General notes

- Don't make any changes to the bootstrap less-files except for variables.less as 
  the files could replaced if a new version of bootstrap is introduced.

- During development and finally for production, the less files must be compiled seperatly (e.g. using CodeKit).


## Update h5bp files:

1) Adapt all html-files to fit to the new version.

2) Adapt css/h5bp/main.css to fit to the new version

3) Check manually what has changed apart from css and html-files (e.g. .htaccess, ...)


## Update ant build tool

1) Make a copy of build/config/project.properties

2) Replace entire build-folder with new version.

3) Check the new project.properties-file to fit to the old one.


## Update bootstrap files:


### Bootstrap less-files

1) Make a copy of less/bootstrap/variables.less .

2) Remove all files inside less/bootstrap/. Be sure no file has been changed manually.

3) Copy all less-files of new bootstrap version into less/bootstrap.

4) Adapt new variables.less to fit to the old copy.


### Bootstrap JS

1) Remove all files inside js/vendor/bootstrap/ .

2) Copy all js-files of the new bootstrap version into js/vendor/bootstrap/ .

3) Copy combined bootstrap.js and bootstrap.min.js into js/vendor/bootstrap/ .

4) Check your html-files if bootstrap js file-names have changed and every file is imported.